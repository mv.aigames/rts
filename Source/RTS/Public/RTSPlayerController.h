// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RTSPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class RTS_API ARTSPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	private:
		void MoveForward(float value);
		void MoveRight(float value);
		void Select();

	protected:
		virtual void SetupInputComponent() override;


};
