// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AbstractBuilding.generated.h"

class UStaticMeshComponent;

UCLASS()
class RTS_API AAbstractBuilding : public AActor
{
	GENERATED_BODY()
	private:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* meshComponent;
	
	public:	
		// Sets default values for this actor's properties
		AAbstractBuilding();

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

	public:	
		// Called every frame
		virtual void Tick(float DeltaTime) override;

};
