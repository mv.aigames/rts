// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Utilities/PickableObject.h"
#include "ExplosivePickableObject.generated.h"


class USceneComponent;

UCLASS()
class RTS_API AExplosivePickableObject : public AActor
{
	GENERATED_BODY()

	private:
		USceneComponent* sceneComponent = nullptr;
		
		UPROPERTY(EditDefaultsOnly, Category = "Pickable Object", meta = (AllowPrivateAccess = "true"))
		TSubclassOf<APickableObject> pickableObjectClass;
		
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Pickable Object", meta = (AllowPrivateAccess = "true"))
		int32 amount = 3;

	protected:
		virtual void BeginPlay() override;

	public:
		AExplosivePickableObject();

		UFUNCTION(BlueprintCallable)
		void Explode();

};
