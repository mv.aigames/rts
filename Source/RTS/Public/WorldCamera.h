// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WorldCamera.generated.h"

class UCameraComponent;

UCLASS()
class RTS_API AWorldCamera : public AActor
{
	GENERATED_BODY()
	
	private:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		UCameraComponent* camera;

	public:	
		// Sets default values for this actor's properties
		AWorldCamera();

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

	public:	
		// Called every frame
		virtual void Tick(float DeltaTime) override;

};
