// Fill out your copyright notice in the Description page of Project Settings.


#include "Building/AbstractBuilding.h"

#include "Components/StaticMeshComponent.h"


// Sets default values
AAbstractBuilding::AAbstractBuilding()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));

	RootComponent = meshComponent;
	Tags.Add("Building");
}

// Called when the game starts or when spawned
void AAbstractBuilding::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAbstractBuilding::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

