// Fill out your copyright notice in the Description page of Project Settings.


#include "MiniMapCamera.h"

// Sets default values
AMiniMapCamera::AMiniMapCamera()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMiniMapCamera::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMiniMapCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

