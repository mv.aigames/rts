// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AbstractUnit.generated.h"


class UCapsuleComponent;
class USkeletalMeshComponent;
class UBehaviorTreeComponent;

UCLASS()
class RTS_API AAbstractUnit : public APawn
{
	GENERATED_BODY()

	private:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
		UCapsuleComponent* capsuleComponent;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
		USkeletalMeshComponent* skeletalComponent;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI", meta = (AllowPrivateAccess = "true"))
		UBehaviorTreeComponent* behaviorTreeComponent;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
		float speed;

		UPROPERTY(BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
		FVector direction;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
		float motion;
	
	public:	
		// Sets default values for this actor's properties
		AAbstractUnit();

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

	public:	
		// Called every frame
		virtual void Tick(float DeltaTime) override;

		UFUNCTION(BlueprintNativeEvent)
		void OnPostTick(float delta);
		virtual void OnPostTick_Implementation(float delta);

		UFUNCTION(BlueprintNativeEvent)
		void OnMoveTo(const FVector& destiny);

		virtual void OnMoveTo_Implementation(const FVector& destiny);

};
