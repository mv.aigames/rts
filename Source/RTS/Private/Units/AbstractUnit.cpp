// Fill out your copyright notice in the Description page of Project Settings.


#include "Units/AbstractUnit.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackBoardComponent.h"

// Sets default values
AAbstractUnit::AAbstractUnit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	capsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	RootComponent = capsuleComponent;

	Tags.Add("Unit");
	
	skeletalComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal"));
	skeletalComponent->SetupAttachment(capsuleComponent);


	// Actor component
	behaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behavior Tree"));
	AddOwnedComponent(behaviorTreeComponent);

}

// Called when the game starts or when spawned
void AAbstractUnit::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAbstractUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	motion = (direction * speed).Size() * DeltaTime;
	OnPostTick(DeltaTime);
}

void AAbstractUnit::OnPostTick_Implementation(float delta)
{

}

void AAbstractUnit::OnMoveTo_Implementation(const FVector& destiny)
{
}

