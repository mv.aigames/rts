// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GodController.generated.h"


class AGod;
/**
 * 
 */
UCLASS()
class RTS_API AGodController : public APlayerController
{
	GENERATED_BODY()
	private:
		void MoveForward(float value);
		void MoveRight(float value);
		void Select();
		AGod* god;

	protected:
		virtual void SetupInputComponent() override;
		virtual void OnPossess(APawn* InPawn) override;
		void BeginPlay() override;
		void ReleaseMouse();
};
