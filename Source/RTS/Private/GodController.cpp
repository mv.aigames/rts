// Fill out your copyright notice in the Description page of Project Settings.


#include "GodController.h"
#include "God.h"


void AGodController::BeginPlay()
{
	Super::BeginPlay();

	ReleaseMouse();
}

void AGodController::ReleaseMouse()
{
	
}

void AGodController::MoveForward(float value)
{
	//UE_LOG(LogTemp, Warning, TEXT("Moving Forward %f"), value);
	god->MoveForward(value);
}

void AGodController::MoveRight(float value)
{
	//UE_LOG(LogTemp, Warning, TEXT("Moving Right %f"), value);
	god->MoveRight(value);
}

void AGodController::Select()
{
	UE_LOG(LogTemp, Warning, TEXT("Selecting"));
	god->Choose(this);
}

void AGodController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &AGodController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AGodController::MoveRight);
	InputComponent->BindAction("MouseLeftClick", EInputEvent::IE_Pressed, this, &AGodController::Select);

	
}

void AGodController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	god = Cast<AGod>(InPawn);
	if (god == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to retrieve god"));
		InputComponent->RemoveActionBinding("MouseLeftClick", EInputEvent::IE_Pressed);
		InputComponent->AxisBindings.Empty();
	}
}