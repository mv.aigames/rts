// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "God.generated.h"

class UCameraComponent;
class USceneComponent;
class APlayerController;

UENUM(BlueprintType)
enum class GodState : uint8
{
	none UMETA(DisplayName = "None"),
	pickingUnit UMETA(DisplayName = "Picking Unit"),
};

UCLASS()
class RTS_API AGod : public APawn
{
	GENERATED_BODY()

	private:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera", meta = (AllowPrivateAccess = "true"))
		UCameraComponent* cameraComponent;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Root", meta = (AllowPrivateAccess = "true"))
		USceneComponent* sceneComponent;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Position", meta = (AllowPrivateAccess = "true"))
		float height = 20.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
		float speed = 100.0f;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug", meta = (AllowPrivateAccess = "true"))
		AActor* pickedActor;

		void ProcessMovement(float delta);
		FVector movement;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "State", meta = (AllowPrivateAccess = "true"))
		GodState state;

public:
	// Sets default values for this pawn's properties
	AGod();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MoveForward(float value);
	void MoveRight(float value);
	void Choose(APlayerController* controller);

	UFUNCTION(BlueprintNativeEvent)
	void OnEntityChosen(AActor* entity);
	virtual void OnEntityChosen_Implementation(AActor* entity);

};
