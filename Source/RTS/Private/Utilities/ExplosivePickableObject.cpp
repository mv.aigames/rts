// Fill out your copyright notice in the Description page of Project Settings.


#include "Utilities/ExplosivePickableObject.h"

#include "Engine/World.h"

// Sets default values
AExplosivePickableObject::AExplosivePickableObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	sceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	RootComponent = sceneComponent;

}

// Called when the game starts or when spawned
void AExplosivePickableObject::BeginPlay()
{
	Super::BeginPlay();
	
}

void AExplosivePickableObject::Explode()
{
	if (!pickableObjectClass)
		return;

	for (int index = 0; index < amount; index++)
	{
		GetWorld()->SpawnActor<APickableObject>(pickableObjectClass, GetActorLocation(), FRotator());
	}
}
