// Fill out your copyright notice in the Description page of Project Settings.


#include "God.h"

#include "Components/SceneComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
//#include "GameFramework/PlayerController.h"
#include "DrawDebugHelpers.h"
#include "BaseGameMode.h"
#include "Units/AbstractUnit.h"


// Sets default values
AGod::AGod()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	sceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(sceneComponent);

	cameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	cameraComponent->SetupAttachment(sceneComponent);
	//SetRootComponent(cameraComponent);
}


// Called when the game starts or when spawned
void AGod::BeginPlay()
{
	Super::BeginPlay();
	ABaseGameMode* gm = Cast<ABaseGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	AActor* playerStart = gm->FindPlayerStart(GetWorld()->GetFirstPlayerController());
	SetActorLocation(playerStart->GetActorLocation() + FVector(0, 0, height));
	state = GodState::none;
}

// Called every frame
void AGod::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ProcessMovement(DeltaTime);
}

// Called to bind functionality to input
void AGod::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AGod::MoveForward(float value)
{
	movement.X = value;
}

void AGod::MoveRight(float value)
{
	movement.Y = value;
}

void AGod::ProcessMovement(float delta)
{
	AddActorLocalOffset(movement * speed * delta);
}

void AGod::Choose(APlayerController* controller)
{
	FVector mousePosition;
	FVector mouseDirection;

	controller->DeprojectMousePositionToWorld(mousePosition, mouseDirection);

	FHitResult hit;

	FVector endPosition = mousePosition + mouseDirection * 1000.0f;
	//UE_LOG(LogTemp, Warning, TEXT("Start Position: %s, End Position: %s"), *mousePosition.ToString(), *endPosition.ToString());

	FCollisionQueryParams params;

	bool b = GetWorld()->LineTraceSingleByChannel(hit, mousePosition, endPosition, ECollisionChannel::ECC_WorldDynamic, params);
	//DrawDebugLine(GetWorld(), mousePosition, endPosition, FColor::Cyan, false, 5.0f);

	if (hit.GetActor())
	{
		OnEntityChosen(hit.GetActor());
	}

	if (state == GodState::none)
	{
		if (hit.GetActor() && hit.GetActor()->Tags.Contains("Unit"))
		{
			//UE_LOG(LogTemp, Warning, TEXT("You have chosen %s actor"), *hit.GetActor()->GetName());
			pickedActor = hit.GetActor();
			state = GodState::pickingUnit;
		}
	}
	else if (state == GodState::pickingUnit)
	{
		if (pickedActor)
		{
			AAbstractUnit* unit = Cast<AAbstractUnit>(pickedActor);
			if (unit)
			{
				unit->OnMoveTo(hit.ImpactPoint);
			}

			pickedActor = nullptr;
			state = GodState::none;
		}
	}
}


void AGod::OnEntityChosen_Implementation(AActor* entity)
{
}
